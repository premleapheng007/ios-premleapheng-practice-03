//
//  AppleLoginViewController.swift
//  Practice-03
//
//  Created by BTB_015 on 11/27/20.
//

import UIKit
import AuthenticationServices

class AppleLoginViewController: UIViewController {
    
    let acessor = AppleSignInClient()
    let loginBtn = ASAuthorizationAppleIDButton()


    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @objc func signInWithApple(sender: ASAuthorizationAppleIDButton) {
        acessor.handleAppleIdRequest(block: { fullName, email, token in
               // receive data in login class.
   })
       }

    @IBAction func appleSignIn(_ sender: Any) {
        loginBtn.addTarget(self, action: #selector(signInWithApple(sender: )), for: .touchUpInside)
        loginBtn.sendActions(for: .touchUpInside)

    }
    


}
