//
//  ViewController.swift
//  Practice-03
//
//  Created by BTB_015 on 11/26/20.
//

import UIKit
import FBSDKLoginKit

class ViewController: UIViewController{
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var email: UILabel!
    @IBOutlet weak var profile: UIImageView!
    
    let loginButton =  FBLoginButton(frame: .zero, permissions: [.publicProfile])

    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.delegate = self
        loginButton.isHidden = true
        
        if let token = AccessToken.current,
               !token.isExpired {
        }else{

        loginButton.permissions = ["public_profile", "email"]

        }
    }
    
    @IBAction func registerWithFacebook(_ sender: Any) {
        loginButton.sendActions(for: .touchUpInside)
    }
}


extension ViewController : LoginButtonDelegate{
    
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
          if let token = AccessToken.current, !token.isExpired{
              let token = token.tokenString
              let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "email, picture.type(large), name, id"], tokenString: token, version: nil, httpMethod: .get)
              request.start(completionHandler: { connection, result, error in

                  let myData = result as? [String: Any]
                  self.email.text = myData!["email"]! as? String
                  self.name.text = myData!["name"]! as? String
                
                  let imageURL = ((myData?["picture"] as? [String: Any])?["data"] as? [String: Any])?["url"] as? String
                  let uri = URL(string: imageURL!)
                  let dataUrl = try? Data(contentsOf: uri!)
                  let img = UIImage(data: dataUrl!)
                  self.profile.image = img
              })
          }
      }
      func loginButtonDidLogOut(_ loginButton: FBLoginButton) {

      }

    
}

