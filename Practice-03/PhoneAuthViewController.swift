//
//  PhoneAuthViewController.swift
//  Practice-03
//
//  Created by BTB_015 on 11/27/20.
//

import UIKit
import FirebaseAuth
import FirebaseCore

class PhoneAuthViewController: UIViewController {

    @IBOutlet weak var verifyCode: UITextField!
    @IBOutlet weak var phoneNumber: UITextField!
    var verification_id: String? = nil
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func phoneLogin(_ sender: Any) {
        if !verifyCode.isHidden {
            if !phoneNumber.text!.isEmpty{
                Auth.auth().settings?.isAppVerificationDisabledForTesting = false
                   PhoneAuthProvider.provider().verifyPhoneNumber(phoneNumber.text!, uiDelegate: nil, completion: {
                       verificationID, error in
                      if(error != nil){
                           return
                       }
                       else{
       
                          self.verification_id = verificationID
                          self.textFieldCode.isHidden = false
                       }
                     })
            }
            else{
                print(" Enter Your Mobile Number")
            }
        }else{
            if verification_id != nil{
                let credential = PhoneAuthProvider.provider().credential(withVerificationID: verification_id!, verificationCode: verifyCode.text!)
                Auth.auth().signIn(with: credential, completion: {authData, error in
                    if (error != nil){
                        print(error.debugDescription)
                    }
                    else{
                        print("Authentication Success With - " + (authData?.user.phoneNumber! ?? "No Phone Number"))
                    }
                })
            }
            else{
                print("Error Getting Verification ID")
            }
         }
    }
}
